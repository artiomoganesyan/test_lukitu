import { NextFunction, Request, Response } from "express";
import { verifyToken } from "../config/jwtUtils";
import redisClient from "../config/redis";
import HttpError from "../utils/HttpError";

export default async (req: Request, res: Response, next: NextFunction) => {
  try {
    const authHeader = req.headers.authorization;
    const token = authHeader && authHeader.split(" ")[1];
    if (!token) {
      throw new HttpError(401, "Unauthorized");
    }
    const decode = verifyToken(token);
    if (typeof decode !== "object") {
      throw new HttpError(418, "How did you get a string??");
    }
    const session = await redisClient.get(`session:${decode.userId}`);
    if (!session) {
      throw new HttpError(401, "Unauthorized");
    }
    req.user = decode;
    next();
  } catch (error) {
    next(error);
  }
};
