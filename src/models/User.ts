import mongoose from "mongoose";
import crypto from "crypto";

const UserSchema = new mongoose.Schema({
  nickName: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  urls: [
    {
      longURL: {
        type: String,
        required: true,
      },
      shortURL: {
        type: String,
        required: true,
      },
    },
  ],
});

UserSchema.index({ "urls._id": 1 });

export const User = mongoose.model("User", UserSchema);
