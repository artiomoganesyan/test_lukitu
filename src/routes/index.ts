import { Router } from "express";
import { authRouter, authPath } from "./auth.route";
import { urlPath, urlRouter } from "./url.router";
import { redirectPath, redirectRouter } from "./redirect.router";

export type RouterDefinition = [string, Router];

const routes: RouterDefinition[] = [
  [authPath, authRouter],
  [urlPath, urlRouter],
  [redirectPath, redirectRouter],
];

export default routes;
