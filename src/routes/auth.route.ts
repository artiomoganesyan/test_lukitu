import { Router } from "express";
import authMiddleware from "../middlewares/authMiddleware";
import authController from "../controllers/auth.controller";

export const authRouter = Router();
export const authPath = "/auth";

authRouter.get("/token", authController.getAccessToken);
authRouter.post("/login", authController.login);
authRouter.delete("/logout", authMiddleware, authController.logout);
