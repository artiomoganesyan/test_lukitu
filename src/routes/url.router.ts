import { Router } from "express";
import authMiddleware from "../middlewares/authMiddleware";
import urlController from "../controllers/url.controller";

export const urlRouter = Router();
export const urlPath = "/url";

urlRouter.get("/", authMiddleware, urlController.get);
urlRouter.post("/", authMiddleware, urlController.create);
urlRouter.patch("/:shortId", authMiddleware, urlController.remove);
