import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import redisClient from "./redis";

dotenv.config();

const JWT_SECRET: string = process.env.JWT_SECRET || "jwt_secret";
const REFRESH_TOKEN_SECRET: string =
  process.env.REFRESH_TOKEN_SECRET || "refresh_token";

// CREATE TOKENS
// ======================================
export const generateTokens = (userId: string) => {
  const is_dev = process.env.NODE_ENV === "development";
  const expiresIn = is_dev ? "1d" : "15m";
  const accessToken = jwt.sign({ userId }, JWT_SECRET, { expiresIn });
  const refreshToken = jwt.sign({ userId }, REFRESH_TOKEN_SECRET, {
    expiresIn: "7d",
  });
  return { accessToken, refreshToken };
};

// VERIFY TOKENS
// ======================================
export const verifyToken = (token: string) => jwt.verify(token, JWT_SECRET);
export const verifyRefreshToken = (token: string) =>
  jwt.verify(token, REFRESH_TOKEN_SECRET);

// TOKEN MANIPULATION
// ======================================
export const setToken = (userId: string, token: string) =>
  redisClient.set(`session:${userId}`, JSON.stringify({ token }), {
    EX: 7 * 24 * 60 * 60,
  });

export const getToken = (userId: string) =>
  redisClient.get(`session:${userId}`);

export const removeToken = (userId: string) =>
  redisClient.del(`session:${userId}`);
