import express, {
  Express,
  NextFunction,
  Request,
  Response,
  Router,
} from "express";
import dotenv from "dotenv";
import routes, { RouterDefinition } from "./routes";
import HttpError from "./utils/HttpError";
import mongoose from "./config/mongo";

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 9500;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose
  .then(() => console.log("MongoDB connected"))
  .catch((err) => console.log(err));

routes.forEach(([path, router]: RouterDefinition) => {
  app.use(`/api${path}`, router);
});

app.get("*", (req: Request, res: Response) => {
  res.status(404).json({ message: "Invalid route" });
});

app.use((err: HttpError, req: Request, res: Response, next: NextFunction) => {
  res.status(err.status ?? 500).json({ message: err.message });
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
