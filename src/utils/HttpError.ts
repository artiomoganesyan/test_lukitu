export default class HttpError extends Error {
  constructor(public status: number, public message: string) {
    super(message);
    this.status = status || 500;
    console.log("HttpError", this.status, this.message)
  }
}
