import { NextFunction, Request, Response } from "express";
import HttpError from "../utils/HttpError";
import {
  generateTokens,
  getToken,
  removeToken,
  setToken,
  verifyRefreshToken,
} from "../config/jwtUtils";
import { User } from "../models/User";

export default {
  getAccessToken: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { refreshToken } = req.body;
      if (!refreshToken) throw new HttpError(401, "Invalid token");

      const decoded = verifyRefreshToken(refreshToken);
      if (typeof decoded !== "object") {
        throw new HttpError(401, "Invalid token");
      }

      const storedToken = getToken(decoded.userId);
      if (!storedToken || storedToken !== refreshToken) {
        throw new HttpError(401, "Invalid token");
      }

      const { accessToken } = generateTokens(decoded.userId);
      res.json({ accessToken });
    } catch (error) {
      next(error);
    }
  },

  login: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { nickName, password } = req.body;

      let user = await User.findOne({ nickName });

      if (!user) {
        user = await User.create({ nickName, password });
      } else {
        if (user.password !== password) {
          throw new HttpError(400, "Login failed");
        }
      }

      const { accessToken, refreshToken } = generateTokens(user.id);
      await setToken(user.id, refreshToken);
      res.json({ accessToken, refreshToken });
    } catch (error) {
      next(error);
    }
  },

  logout: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { userId } = req.user;
      await removeToken(userId);
      res.json({ message: "Logout successful" });
    } catch (error) {
      next(error);
    }
  },
};
