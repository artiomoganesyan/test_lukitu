import { NextFunction, Request, Response } from "express";
import HttpError from "../utils/HttpError";
import { User } from "../models/User";
import mongoose from "mongoose";

export default {
  redirect: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;
      const result = await User.aggregate([
        { $match: { "urls._id": new mongoose.Types.ObjectId(id) } },
        { $unwind: "$urls" },
        { $match: { "urls._id": new mongoose.Types.ObjectId(id) } },
        { $project: { _id: 0, longURL: "$urls.longURL" } },
      ]).exec();

      if (!result.length) {
        throw new HttpError(404, "URL not found");
      }

      res.redirect(result[0].longURL);
    } catch (error) {
      next(error);
    }
  },
};
