import { NextFunction, Request, Response } from "express";
import crypto from "crypto";
import HttpError from "../utils/HttpError";
import { User } from "../models/User";

export default {
  get: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const urls = await User.findById(req.user.userId, "urls");
      res.json(urls);
    } catch (error) {
      next(error);
    }
  },

  create: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { longURL } = req.body;
      const shortURL =
        (process.env.host ?? "http://localhost/r/") + crypto.randomUUID();

      const user = await User.findById(req.user.userId);
      if (!user) {
        throw new HttpError(404, "User not found");
      }
      user.urls.push({ longURL, shortURL });
      await user.save();
      res.json({ longURL, shortURL });
    } catch (error) {
      next(error);
    }
  },

  remove: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { shortId } = req.params;
      const result = await User.updateOne(
        { _id: req.user.userId },
        { $pull: { urls: { _id: shortId } } }
      );
      if (!result.modifiedCount) {
        throw new HttpError(404, "User not found");
      }
      res.sendStatus(204);
    } catch (error) {
      next(error);
    }
  },
};
